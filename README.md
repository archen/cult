# Container to Firecracker
Convert docker/OCI containers to [Firecracker VMs](https://firecracker-microvm.github.io)

Using Bazel makes it almost like cheating, but you can get the same
result by taking any of your favorite docker images and leveraging
the `docker save` command to create the tarball needed for the steps
below. I'll document that eventually.

## Prereqs
1. Install bazel and gcc (nix users can just run nix-shell from the project root)
2. Install [firecracker](https://github.com/firecracker-microvm/firecracker/releases/download/v0.11.0/firecracker-v0.11.0)
3. Uncompressed Linux Kernel, I used [Amazon's](https://s3.amazonaws.com/spec.ccfc.min/img/hello/kernel/hello-vmlinux.bin)

## Getting it done
Start firecracker per [the instructions](https://github.com/firecracker-microvm/firecracker/blob/master/docs/getting-started.md#running-firecracker).

In a separate shell:

```bash
# Build the container image
bazel build :hello_flat
cp $(bazel info bazel-bin)/hello_flat.* .

# Create and mount the filesystem
dd if=/dev/zero of=hello_flat.ext4 bs=1M count=100
mkfs.ext4 hello_flat.ext4
mkdir deleteme
sudo mount -o loop hello_flat.ext4 deleteme
sudo tar --same-owner -xvf hello_flat.tar -C deleteme

# Create minimal devices needed for booting python
cd deleteme
sudo mknod -m 622 dev/console c 5 1
sudo mknod -m 444 dev/urandom c 1 9
sudo mkdir -v dev/pts
sudo mkdir -v dev/shm

# TODO: the shebang from nixOS is killing my flow, replace it with /usr/bin/python in Baz
vi app/hello_py.binary.runfiles/__main__/hello_py.binary
>> change shebang line to #!/usr/bin/python

# Send and run the image (make sure that you're using the right paths to your ext4 and kernel images)
curl --unix-socket /tmp/firecracker.socket -i \
    -X PUT 'http://localhost/boot-source' \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d '{"kernel_image_path": "./hello-vmlinux.bin","boot_args":"console=ttyS0 reboot=k panic=1 pci=off init=/app/hello_py.binary"}

curl --unix-socket /tmp/firecracker.socket -i \
    -X PUT 'http://localhost/drives/rootfs' \
    -H 'Accept: application/json'           \
    -H 'Content-Type: application/json'     \
    -d '{"drive_id": "rootfs","path_on_host": "./hello_flat.ext4","is_root_device": true,"is_read_only": false}'

curl --unix-socket /tmp/firecracker.socket -i \
    -X PUT 'http://localhost/actions'       \
    -H  'Accept: application/json'          \
    -H  'Content-Type: application/json'    \
    -d '{"action_type": "InstanceStart"}'
```

# TODOs
- Add as many steps to Bazel genrules as possible
- Perhaps cut a generic python /dev mount?
- Fix the damn shebang problem in hello_py.binary 
